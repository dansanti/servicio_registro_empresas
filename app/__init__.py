from flask import Flask, render_template
import flask_login as login
from flask_security import Security, SQLAlchemyUserDatastore
from flask_mail import Mail
from flask_apscheduler import APScheduler


# Create Flask application
app = Flask(__name__)
from . import models
from . import views, config, build_sample_db, api

models.db.app = app
models.db.init_app(app)

# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(models.db, models.user.User, models.role.Role)
security = Security(app, user_datastore)

# Setup mail
mail = Mail(app)

# Flask views
@app.route('/')
def index():
    return render_template('index.html')

scheduler = APScheduler()
    # it is also possible to enable the API directly
    # scheduler.api_enabled = True
scheduler.init_app(app)
scheduler.start()

# Create admin
# Initialize flask-login
def init_login():
    login_manager = login.LoginManager()
    login_manager.init_app(app)

    # Create user loader function
    @login_manager.user_loader
    def load_user(user_id):
        return models.db.session.query(models.user.User).get(user_id)


init_login()
