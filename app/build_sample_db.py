from . import models
from werkzeug.security import generate_password_hash
import csv
import os
import logging
_logger = logging.getLogger(__name__)


def build_sample_db():
    models.db.drop_all()
    models.db.create_all()
    env = models.db.get_tables_for_bind()
    csvs = []
    for root, dirs, files in os.walk("./app/data"):
        csvs = files

    def process_csv(filename):
            c_file = filename.replace('.csv', '')
            if not hasattr(models, c_file):
                _logger.info("no existe %s" % c_file)
                print("no existe %s" % c_file)
                return
            clase_f = c_file.split('_')
            clase = ''
            for c in clase_f:
                clase += c[0].upper() + c[1:]
            file_obj = getattr(models, c_file)
            arch = open('./app/data/%s' % filename).read()
            datos = csv.reader(arch.splitlines(), csv.excel, delimiter=',', quotechar='"')
            i = 0
            table = False
            for t in env:
                if t.name == c_file:
                    table = t
            for row in datos:
                obj = getattr(file_obj, clase)()
                if i == 0:
                    cabecera = row
                    i += 1
                    continue
                j = 0
                for target in cabecera:
                    key = target
                    field = False
                    if len(target.split('/')) > 1:
                        key, field = target.split('/')
                    if not hasattr(obj, key):
                        j += 1
                        _logger.info("clase %s sin campo %s, se ignora" % (clase, key))
                        continue
                    val = row[j]
                    col = False
                    for c in table.columns:
                        if c.name == key:
                            col = c
                    if col.foreign_keys:
                        if field:
                            val = None
                            for r in col.foreign_keys:
                                field_clase_f = r._get_colspec().split('.')[0]
                                if field_clase_f + ".csv" in csvs:
                                    process_csv(field_clase_f + ".csv")
                                    csvs.remove(field_clase_f + ".csv")
                                field_clase = ''
                                for c in field_clase_f.split('.'):
                                    field_clase += c[0].upper() + c[1:]
                                field_obj = getattr(models, field_clase_f)
                                obj_f = getattr(field_obj, field_clase)
                                val_obj = getattr(obj_f, field)
                                recs = models.db.session.query(obj_f).filter(val_obj==row[j]).all()
                                for rec in recs:
                                    val = rec.id
                    else:
                        val = col.type.python_type(row[j])
                    setattr(obj, key, val)
                    j += 1
                models.db.session.add(obj)
                i += 1
    while csvs:
        process_csv(csvs[0])
        del(csvs[0])
    #roles
    admin_role = models.role.Role(name="admin", description="System Group")
    models.db.session.add(admin_role)
    user_role = models.role.Role(name="user", description="User Group")
    models.db.session.add(user_role)
    #user
    test_user = models.user.User(login="test",
                                 password=generate_password_hash("test"),
                                 roles=[admin_role],
                                 email="dansanti@gmail.com")
    models.db.session.add(test_user)
    public_user = models.user.User(login="public",
                                   password=generate_password_hash("public"),
                                   active=False,
                                   roles=[user_role])
    models.db.session.add(public_user)
    public_token = models.token.Token(name="token_publico",
                                      valor='token_publico',
                                      active=True,
                                      usuario=public_user,
                                      usos=200)
    models.db.session.add(public_token)
    #Medios de pago
    models.db.session.add(models.screen.Screen(width=500, height=2000))
    models.db.session.add(models.screen.Screen(width=550, height=1900))

    models.db.session.commit()
