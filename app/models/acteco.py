from . import db


class Acteco(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(200), nullable=False)
    code = db.Column(db.String(20))
    afecto = db.Column(db.String(2))
    active = db.Column(db.Boolean)

    def __str__(self):
        return "[{}] {}".format(self.code, self.name)
