from . import db
import datetime

company_tags_table = db.Table('company_tags_remote', db.Model.metadata,
                           db.Column('company_id', db.Integer, db.ForeignKey('company_info_remote.id')),
                           db.Column('tag_id', db.Integer, db.ForeignKey('tag.id'))
                           )
company_actecos_table = db.Table('company_actecos_remote', db.Model.metadata,
                           db.Column('company_id', db.Integer, db.ForeignKey('company_info_remote.id')),
                           db.Column('acteco_id', db.Integer, db.ForeignKey('acteco.id'))
                           )


class CompanyInfoRemote(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(400), nullable=False)
    nombre_fantasia = db.Column(db.String(400))
    rut = db.Column(db.String(64))
    email = db.Column(db.String(64))
    dte_email = db.Column(db.String(100))
    url = db.Column(db.String(200))
    telefono = db.Column(db.String(64))
    direccion = db.Column(db.String(200))
    numero_resol = db.Column(db.Integer())
    fecha_resol = db.Column(db.Date)
    fecha_auth = db.Column(db.Date)
    glosa_giro = db.Column(db.String(200))
    logo = db.Column(db.Binary)

    acteco_ids = db.relationship('Acteco', secondary=company_actecos_table)
    tag_ids = db.relationship('Tag', secondary=company_tags_table)
    comuna_id = db.Column(db.Integer(), db.ForeignKey('comuna.id'))
    comuna = db.relationship('Comuna', foreign_keys=comuna_id, backref='company_info_remote')
    ciudad_id = db.Column(db.Integer(), db.ForeignKey('ciudad.id'))
    ciudad = db.relationship('Ciudad', foreign_keys=ciudad_id, backref='company_info_remote')
    provincia_id = db.Column(db.Integer(), db.ForeignKey('provincia.id'))
    provincia = db.relationship('Provincia', foreign_keys=provincia_id, backref='company_info_remote')
    region_id = db.Column(db.Integer(), db.ForeignKey('region.id'))
    region = db.relationship('Region', foreign_keys=region_id, backref='company_info_remote')

    active = db.Column(db.Boolean, default=True)
    origen = db.Column(db.String(64))
    fecha_creado = db.Column(db.DateTime, default=datetime.datetime.now())
    actualizado = db.Column(db.DateTime, default=datetime.datetime.now())

    def __str__(self):
        return "{} - {}".format(self.rut, self.name)
