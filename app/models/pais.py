from . import db


class Pais(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(64), nullable=False)
    code = db.Column(db.String(64))
    active = db.Column(db.Boolean)

    def __str__(self):
        return "[{}] {}".format(self.code, self.name)
