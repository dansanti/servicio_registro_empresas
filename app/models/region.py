from . import db
from app.models.pais import Pais


class Region(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(64), nullable=False)
    code = db.Column(db.String(64))
    prefijo = db.Column(db.String(3))
    pais_id = db.Column(db.Integer(), db.ForeignKey(Pais.id))
    pais = db.relationship(Pais, foreign_keys=pais_id, backref='region_info')
    active = db.Column(db.Boolean, default=True)

    def __str__(self):
        return "[{}] {}".format(self.code, self.name)
