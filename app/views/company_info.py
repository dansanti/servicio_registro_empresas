from app.models import db
from app.models.company_info import CompanyInfo
from app.models.tag import Tag
from .admin import admin
from .my_model_view import MyModelView
from flask_admin.contrib.sqla import filters
from wtforms import validators
import flask_login as login


class CompanyAdmin(MyModelView):

    def is_accessible(self):
        return login.current_user.is_authenticated and login.current_user.has_role('admin')

    column_list = ['id', 'rut', 'name', 'dte_email', 'acteco_ids', 'actualizado', 'consultas']
    column_default_sort = ('id', True)
    column_sortable_list = [
        'id',
        'rut',
        'name',
        'consultas'
        #('user', ('user.last_name', 'user.first_name')),  # sort on multiple columns
    ]
    column_labels = dict(title='Company Name')  # Rename 'title' column in list view
    column_searchable_list = [
        'name',
        'tag_ids.name',
        'rut',
        'dte_email',
    ]
    column_labels = {
            'name': 'Name',
            'tag_ids.name': 'Actecos'
    }
    column_filters = [
        #'user',
        'tag_ids',
        filters.FilterLike(CompanyInfo.name, 'Fixed Title', options=(('test1', 'Test 1'), ('test2', 'Test 2'))),
    ]
    can_export = True
    export_max_rows = 1000
    export_types = ['csv', 'xls']

    # Pass arguments to WTForms. In this case, change label for text field to
    # be 'Big Text' and add required() validator.
    form_args = dict(
                    text=dict(label='Big Text', validators=[validators.required()])
                )

    form_ajax_refs = {
        'tag_ids': {
            'fields': (Tag.name,),
            'minimum_input_length': 0,  # show suggestions, even before any user input
            'placeholder': 'Please select',
            'page_size': 5,
        },
    }

    def __init__(self, session):
        # Just call parent class with predefined model.
        super(CompanyAdmin, self).__init__(CompanyInfo, session)


admin.add_view(CompanyAdmin(db.session))
