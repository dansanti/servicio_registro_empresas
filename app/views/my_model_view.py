from flask_admin.contrib import sqla
import flask_login as login
from flask_admin import form
from wtforms import ValidationError
from werkzeug.datastructures import FileStorage
from base64 import b64encode


try:
    from wtforms.fields.core import _unset_value as unset_value
except ImportError:
    from wtforms.utils import unset_value


# Create customized model view class
class MyModelView(sqla.ModelView):

    def is_accessible(self):
        return login.current_user.is_authenticated


# Fields
class FileUploadB64(form.FileUploadField):
    """
        Customizable file-upload field.

        Saves file to field binary, handles updates and deletions. Inherits from `StringField`,
        resulting filename will be stored as string.
    """
    widget = form.FileUploadInput()

    def __init__(self, label=None, validators=None,
                 column_name=None, namegen=None, allowed_extensions=None,
                 **kwargs):
        """
            Constructor.

            :param label:
                Display label
            :param validators:
                Validators
            :param field_name:
                Column where name is set
            :param namegen:
                Function that will generate filename from the model and uploaded file object.
                Please note, that model is "dirty" model object, before it was committed to database.

                For example::

                    import os.path as op

                    def prefix_name(obj, file_data):
                        parts = op.splitext(file_data.filename)
                        return secure_filename('file-%s%s' % parts)

                    class MyForm(BaseForm):
                        upload = FileUploadField('File', namegen=prefix_name)

            :param allowed_extensions:
                List of allowed extensions. If not provided, will allow any file.
        """
        self.column_name = column_name or 'name'

        self.namegen = namegen or form.namegen_filename
        self.allowed_extensions = allowed_extensions

        super(FileUploadB64, self).__init__(label, validators, **kwargs)

    def is_file_allowed(self, filename):
        """
            Check if file extension is allowed.

            :param filename:
                File name to check
        """
        if not self.allowed_extensions:
            return True

        return ('.' in filename and
                filename.rsplit('.', 1)[1].lower() in
                map(lambda x: x.lower(), self.allowed_extensions))

    def _is_uploaded_file(self, data):
        return (data and isinstance(data, FileStorage) and data.filename)

    def pre_validate(self, form):
        if self._is_uploaded_file(self.data) and not self.is_file_allowed(self.data.filename):
            raise ValidationError(gettext('Invalid file extension'))

        # Handle overwriting existing content
        if not self._is_uploaded_file(self.data):
            return

    def process(self, formdata, data=unset_value):
        if formdata:
            marker = '_%s-delete' % self.name
            if marker in formdata:
                self._should_delete = True

        return super(FileUploadB64, self).process(formdata, data)

    def process_formdata(self, valuelist):
        if self._should_delete:
            self.data = None
        elif valuelist:
            for data in valuelist:
                if self._is_uploaded_file(data):
                    self.data = data
                    break

    def generate_name(self, obj, file_data):
        filename = self.namegen(obj, file_data)
        return filename

    def populate_obj(self, obj, name):
        if self._is_uploaded_file(self.data):
            filename = self.generate_name(obj, self.data)
            self.data.filename = filename

            setattr(obj, name, b64encode(self.data.read()))
            setattr(obj, self.column_name, getattr(obj, self.column_name, None) or filename)