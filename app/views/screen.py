from app.models import db
from app.models.screen import Screen
from .admin import admin
from .my_model_view import MyModelView


class ScreenView(MyModelView):
    column_list = ['id', 'width', 'height', 'number_of_pixels']  # not that 'number_of_pixels' is a hybrid property, not a field
    column_sortable_list = ['id', 'width', 'height', 'number_of_pixels']

    # Flask-admin can automatically detect the relevant filters for hybrid properties.
    column_filters = ('number_of_pixels', )


admin.add_view(ScreenView(Screen, db.session, category="Other"))
