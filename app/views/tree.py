from app.models import db
from app.models.tree import Tree
from .admin import admin
from .my_model_view import MyModelView


class TreeView(MyModelView):
    form_excluded_columns = ['children', ]


admin.add_view(TreeView(Tree, db.session, category="Other"))
