from app.models import db
from app.models.user import User
from app.models.user_info import UserInfo
from .my_model_view import MyModelView
from .admin import admin
import flask_login as login
from flask_admin.actions import action
from flask_security import roles_required
from flask_admin.babel import lazy_gettext


class UserAdmin(MyModelView):

    def is_accessible(self):
        return login.current_user.is_authenticated and login.current_user.has_role('admin')

    column_default_sort = ('id', True)

    form_excluded_columns = ('date', 'token')
    column_exclude_list = ('date', 'password')

    column_searchable_list = [
        'login',
        'tokens.name',
        'first_name',
        'email',
    ]

    @action('payment',
            lazy_gettext('Payment email'),
            lazy_gettext('Are you sure you want to send payment selected records?'))
    def send_payment(self, ids):
        for pk in ids:
            user = self.get_one(pk)
            user.send_payment()


admin.add_view(UserAdmin(User, db.session))
admin.add_view(MyModelView(UserInfo, db.session, category="Other"))
